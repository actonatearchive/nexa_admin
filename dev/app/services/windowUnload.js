define([
  'app-bootstrap'
], function() {
  angular
    .module('act.Services')
    .factory('window', window);
  DialogService.$inject = ['$window','$rootscope'];
  function window($window, $rootscope) {
    var log = logger.log().child('Window');
    return {
      onbeforeunload: onbeforeunload,

    };
    $window.onbeforeunload = function(e) {
      var confirmation = {};
      var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
      if (event.defaultPrevented) {
        return confirmation.message;
      }
    };

    $window.onunload = function() {
      $rootScope.$broadcast('onUnload');
    };

  }
});
