/**
 * @namespace act.Main
 */
define([
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Dealers')
      .controller('MainDealersBaseController', controller);

    controller.$inject = ['$rootScope','$state', 'logger', 'ToastService','DialogService','Rest','$scope','Upload','APP_REST_URL'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($rootScope, $state, logger, ToastService, DialogService, Rest, $scope, Upload,APP_REST_URL) {
    var ViewModel = this;
    var log = logger.log().child('MainDealersBaseController');

    //     window.onbeforeunload = function(e) {
    //     e = e || window.event;
    //     e.preventDefault = true;
    //     e.cancelBubble = true;
    //     e.returnValue = 'test';
    // };
    $rootScope.pageTitle = 'Dealers';

    ViewModel.showAddDialog = showAddDialog;
    ViewModel.showUpdateDialog = showUpdateDialog;
    ViewModel.toggleActiveDealer = toggleActiveDealer;

    ViewModel.selected = [];

    ViewModel.loadPromise = false;
    ViewModel.dealers = [];

    ViewModel.getDealers = function() {
        var httpResource = Rest.resource('dealer').get('getAllDealers');

        httpResource().then(function(resp) {
           log.info('in resolve ', resp);
           ViewModel.dealers = resp.data;
           console.log("dealers: ", resp);
          })
          .catch(function(err) {
            log.info('in reject ', err);
          });
    };


    ViewModel.add = function(dealer) {

        var obj = {
            id: dealer.id,
            name: dealer.name,
            email: dealer.email,
            mobile: dealer.mobile,
            address: {
                line1: dealer.address.line1,
                line2: dealer.address.line2,
                landmark: dealer.address.landmark,
                pincode: dealer.address.pincode,
                city: dealer.address.city,
                state: dealer.address.state,
                country: 'India'
            },
            isActive: 1
        };

        // console.log("OPTIONS..",car);
        var httpResource = Rest.resource('dealer').post('createDealer');

        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    };


    function showAddDialog(event) {

        DialogService.show({
            controller: 'MainDealersBaseController',
            controllerPath: 'app/main-dealers/ctrl/base.ctrl.js',
            templatePath: 'app/main-dealers/tpl/addDealer.tpl.html',
            options: {
              targetEvent: event
            }
        });
    }

    function showUpdateDialog(event, db) {

        DialogService.show({
            controller: 'MainDealersController',
            controllerPath: 'app/main-dealers/ctrl/addDealer.ctrl.js',
            templatePath: 'app/main-dealers/tpl/addDealer.tpl.html',
            options: {
              targetEvent: event
            },locals: db
        });
    }

    //Active/Deactivate Maker
    function toggleActiveDealer(car_id, _isActive) {
        var obj = {
            id: car_id,
            isActive: _isActive
        };

        var httpResource = Rest.resource('dealer').post('updateDealer');
        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
        DialogService.cancel();
    }

  }

  });
