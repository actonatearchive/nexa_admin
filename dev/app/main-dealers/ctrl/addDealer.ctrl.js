/**
 * @namespace act.Main
 */
define([
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Dealers')
      .controller('MainDealersController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService','DialogService','localsData', 'Rest', 'Upload', 'APP_REST_URL', 'APP_IMAGE_URL'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * MainCars Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, DialogService, localsData, Rest, Upload, APP_REST_URL, APP_IMAGE_URL) {
    var ViewModel = this;
    ViewModel.dealer = localsData;
    ViewModel.update = update;

    ViewModel.loadPromise = false;

    function update(dealer) {
        console.log("updateDealer()", dealer);
        var obj = {
            id: dealer.id,
            name: dealer.name,
            email: dealer.email,
            mobile: dealer.mobile,
            address: {
                line1: dealer.address.line1,
                line2: dealer.address.line2,
                landmark: dealer.address.landmark,
                pincode: dealer.address.pincode,
                city: dealer.address.city,
                state: dealer.address.state,
                country: 'India'
            }
        };

        var httpResource = Rest.resource('dealer').post('updateDealer');
        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
      DialogService.cancel();
    }

  }

  });
