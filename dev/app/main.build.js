/**
 * @namespace act
 */
define([
  // Base Config & Req
  'app-bootstrap',

  // Modules Configs
  'app/base/base.config',
  'app/auth/auth.config',
  'app/main/main.config',
  'app/main-admins/main-admins.config',
  'app/main-makers/main-makers.config',
  'app/main-cars/main-cars.config',
  'app/main-dealers/main-dealers.config',

], function() {

  angular.module('act', [
    'act.Base',
    'act.Globals',

    // All V2 Modules
    'act.Main',
    'act.Auth',

    //Sub
    'act.Main.Admins',
    'act.Main.Makers',
    'act.Main.Cars',
    'act.Main.Dealers',

    // All 3rd party modules
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'md.data.table',
    'ngFileUpload',

  ])
  .config(NgMaterialConfig);


  // /////////////////////////////////////

  /*=====================================
  =            Config Blocks            =
  =====================================*/

  /*=====  End of Config Blocks  ======*/

    NgMaterialConfig.$inject = ['$mdThemingProvider'];

    /**
     * Set theme colors for Angular Material
     *
     * @public
     *
     * @memberof   act
     *
     * @author     shoaibmerchant
     *
     * @class      NgMaterialConfig
     * @param      {Object}  $mdThemingProvider  Angular Material Theming provider
     */
    function NgMaterialConfig($mdThemingProvider) {

      $mdThemingProvider.definePalette('appPrimaryPalette', {
        '50': 'ECEFF1',
        '100': 'CFD8DC',
        '200': 'B0BEC5',
        '300': '90A4AE',
        '400': '78909C',
        '500': '607D8B',
        '600': '546E7A',
        '700': '455A64',
        '800': '37474F',
        '900': '263238',
        'A100': '607D8B',
        'A200': 'FFAB40',
        'A400': 'FF9100',
        'A700': 'FF6D00',
        'contrastDefaultColor': 'light',
        'contrastLightColors': ['50', '100',
          '200', '300', '400', 'A100'
        ],
      });

      $mdThemingProvider.theme('default')
        .primaryPalette('appPrimaryPalette')
        .accentPalette('blue-grey');

      $mdThemingProvider.theme('successTheme')
        .primaryPalette('green')
        .accentPalette('green');
    }
});
