/**
 * @namespace act.Main
 */
define([
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Makers')
      .controller('MainMakersController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService','DialogService','localsData', 'Rest', 'Upload', 'APP_REST_URL', 'APP_IMAGE_URL'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * MainMakers Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, DialogService, localsData, Rest, Upload, APP_REST_URL, APP_IMAGE_URL) {
    var ViewModel = this;
    ViewModel.maker = localsData;
    ViewModel.update = update;
    ViewModel.updatePhoto = updatePhoto;
    ViewModel.loadPromise = false;
    ViewModel.photo = {};

    ViewModel.photo = APP_IMAGE_URL + ViewModel.maker.primary_image.sm;
    console.log("primary_image: ", ViewModel.maker.primary_image.sm);
    function update(maker) {
        console.log("updateMaker()", maker);
        var obj = {
            id: maker.id,
            name: maker.name
        };

        var httpResource = Rest.resource('maker').post('updateMaker');
        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    }

    //Uploader
    function initWatch() {
        $scope.$watch('ViewModel.photo', function() {
            console.log("WATCHER: ", ViewModel.photo);
            if (ViewModel.photo && ViewModel.photo.name !== undefined) {
                ViewModel.photoSelected = true;
                ToastService.info("Photo has been selected.");
            }
        });
    }

    function updatePhoto(maker) {
        ToastService.loading();
        ViewModel.photoUploadInProgress = true;
        Upload.upload({
            url: APP_REST_URL +  '/maker/updateMakerPhoto',
            data: {
                file: ViewModel.photo,
                id: maker.id
            }
        })
        .then(function(resp) {
            console.log("Resp:- ", resp);
            console.log('File uploaded successfully', resp.data);

            ToastService.success(resp.data.message);
            ToastService.hide();
            ViewModel.closeDialog();
            $state.reload();
        })
        .catch(function(err) {
            log.error('Error uploading file', err);
            ToastService.error('Error uploading file');
        })
        .finally(function() {
            ViewModel.photoUploadInProgress = false;
        });

    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
      DialogService.cancel();
    }

  }

  });
