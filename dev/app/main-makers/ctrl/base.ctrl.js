/**
 * @namespace act.Main
 */
define([
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Makers')
      .controller('MainMakersBaseController', controller);

    controller.$inject = ['$rootScope','$state', 'logger', 'ToastService','DialogService','Rest','$scope','Upload','APP_REST_URL'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($rootScope, $state, logger, ToastService, DialogService, Rest, $scope, Upload,APP_REST_URL) {
    var ViewModel = this;
    var log = logger.log().child('MainMakersBaseController');

    //     window.onbeforeunload = function(e) {
    //     e = e || window.event;
    //     e.preventDefault = true;
    //     e.cancelBubble = true;
    //     e.returnValue = 'test';
    // };
    $rootScope.pageTitle = 'Makers';

    ViewModel.showAddDialog = showAddDialog;
    ViewModel.showUpdateDialog = showUpdateDialog;
    ViewModel.toggleActiveMaker = toggleActiveMaker;

    ViewModel.selected = [];

    ViewModel.loadPromise = false;
    ViewModel.makers = [];
    ViewModel.photo = {};
    ViewModel.getMakers = function() {
        var httpResource = Rest.resource('').get('getAllMakers');

        httpResource().then(function(resp) {
           log.info('in resolve ', resp);
           ViewModel.makers = resp.data;
           console.log("MAKERS: ", resp);
          })
          .catch(function(err) {
            log.info('in reject ', err);
          });
    };

    ViewModel.add = function(maker) {
        // console.log("MAKER GOES HERE..",maker);

        Upload.upload({
            url: APP_REST_URL +  '/maker/createMaker',
            data: {
                file: ViewModel.photo,
                name: maker.name,
                isActive: 1
            }
        })
        .then(function(resp) {
            console.log("Resp:- ", resp);
            console.log('File uploaded successfully', resp.data);

            ToastService.success(resp.data.message);
            ToastService.hide();
            ViewModel.closeDialog();
            $state.reload();
        })
        .catch(function(err) {
            log.error('Error uploading file', err);
            ToastService.error('Error uploading file');
        })
        .finally(function() {
            ViewModel.photoUploadInProgress = false;
        });


        // var httpResource = Rest.resource('maker').post('createMaker');
        //
        // httpResource(obj).then(function(resp) {
        //         if (resp.data.status === 'success') {
        //             ToastService.success(resp.data.message);
        //             ViewModel.closeDialog();
        //             $state.reload();
        //         } else {
        //             ToastService.error(resp.data.message);
        //         }
        //     })
        //     .catch(function(err) {
        //         log.info('in reject ', err);
        //     });
    };

    //Uploader
    function initWatch() {
        $scope.$watch('ViewModel.photo', function() {
            console.log("WATCHER: ", ViewModel.photo);
            if (ViewModel.photo && ViewModel.photo.name !== undefined) {
                ViewModel.photoSelected = true;
                ToastService.info("Photo has been selected.");
            }
        });
    }



    // function startUpload() {
    //     log.info('Starting upload for file', ViewModel.photo);
    //     ViewModel.photoUploadInProgress = true;
    //     ToastService.loading();
    //     Upload.upload({
    //         url: APP_REST_URL +  '/maker/addMaker',
    //         data: {
    //             file: ViewModel.photo
    //         }
    //     })
    //   .then(function(res) {
    //     console.log("res:", res);
    //     ViewModel.url="https://deliverydon-dev.s3-ap-southeast-1.amazonaws.com/catalog/xs_"+res.data.imagePath;
    //
    //     console.log('File uploaded successfully', res.data);
    //     // path_array = (res.data.data[0].fd).split('/');
    //     // = path_array[7] + '/' + path_array[8] || ""
    //     ToastService.hide();
    //   })
    //   .catch(function(err) {
    //     log.error('Error uploading file', err);
    //     ToastService.error('Error uploading file');
    //   })
    //   .finally(function() {
    //     ViewModel.photoUploadInProgress = false;
    //   });
    // }

    //CallBacks
    initWatch();
    function showAddDialog(event) {

        DialogService.show({
            controller: 'MainMakersBaseController',
            controllerPath: 'app/main-makers/ctrl/base.ctrl.js',
            templatePath: 'app/main-makers/tpl/addMaker.tpl.html',
            options: {
              targetEvent: event
            }
        });
    }

    function showUpdateDialog(event, db) {

        DialogService.show({
            controller: 'MainMakersController',
            controllerPath: 'app/main-makers/ctrl/addMaker.ctrl.js',
            templatePath: 'app/main-makers/tpl/addMaker.tpl.html',
            options: {
              targetEvent: event
            },locals: db
        });
    }


    //Active/Deactivate Maker
    function toggleActiveMaker(maker_id, _isActive) {

        var obj = {
            id: maker_id,
            isActive: _isActive
        };

        var httpResource = Rest.resource('maker').post('updateMaker');
        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
        DialogService.cancel();
    }

  }

  });
