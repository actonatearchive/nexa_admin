/**
 * @namespace act.Main
 */
define([
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Cars')
      .controller('MainCarsController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService','DialogService','localsData', 'Rest', 'Upload', 'APP_REST_URL', 'APP_IMAGE_URL'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * MainCars Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, DialogService, localsData, Rest, Upload, APP_REST_URL, APP_IMAGE_URL) {
    var ViewModel = this;
    ViewModel.car = localsData;
    ViewModel.update = update;
    ViewModel.updatePhoto = updatePhoto;
    ViewModel.loadPromise = false;
    ViewModel.photo = {};

    if (ViewModel.car.primary_image !== undefined) {
        ViewModel.photo = APP_IMAGE_URL + ViewModel.car.primary_image.sm;
        console.log("primary_image: ", ViewModel.car.primary_image.sm);
    }
    function update(car) {
        console.log("updateCar()", car);
        var obj = {
            id: car.id,
            car_name: car.car_name
        };

        var httpResource = Rest.resource('car').post('updateCar');
        httpResource(obj).then(function(resp) {
                if (resp.data.status === 'success') {
                    ToastService.success(resp.data.message);
                    ViewModel.closeDialog();
                    $state.reload();
                } else {
                    ToastService.error(resp.data.message);
                }
            })
            .catch(function(err) {
                log.info('in reject ', err);
            });
    }
    //getMakers
    function getMakers() {
        var httpResource = Rest.resource('').get('getAllMakers');

        httpResource().then(function(resp) {
           ViewModel.makers = resp.data;
           console.log("MAKERS: ", ViewModel.makers);
          })
          .catch(function(err) {
            console.log('in reject ', err);
          });
    }
    //Uploader
    function initWatch() {
        $scope.$watch('ViewModel.photo', function() {
            console.log("WATCHER: ", ViewModel.photo);
            if (ViewModel.photo && ViewModel.photo.name !== undefined) {
                ViewModel.photoSelected = true;
                ToastService.info("Photo has been selected.");
            }
        });
    }

    function updatePhoto(car) {
        ToastService.loading();
        ViewModel.photoUploadInProgress = true;
        Upload.upload({
            url: APP_REST_URL +  '/car/updateMakerPhoto',
            data: {
                file: ViewModel.photo,
                id: car.id
            }
        })
        .then(function(resp) {
            console.log("Resp:- ", resp);
            console.log('File uploaded successfully', resp.data);

            ToastService.success(resp.data.message);
            ToastService.hide();
            ViewModel.closeDialog();
            $state.reload();
        })
        .catch(function(err) {
            log.error('Error uploading file', err);
            ToastService.error('Error uploading file');
        })
        .finally(function() {
            ViewModel.photoUploadInProgress = false;
        });

    }
    //GetMakers
    getMakers();
    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
      DialogService.cancel();
    }

  }

  });
