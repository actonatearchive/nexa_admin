define([
  'app-bootstrap'
],function() {

  angular.module('act.Main.Admins')
    .factory('AdminsService', AdminsService);

  AdminsService.$inject = ['logger', 'Rest', '$q', '$timeout'];

  function AdminsService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('AdminsService');

    return {
      get: get,
      getAdmins: getUsers,
      add: add,
      update: update,
    };

    /////////////////////////////////////////////////////////

    function get(options) {
      var deferred = $q.defer();

      var httpResource = Rest.resource('user').get('session');

      $timeout(function() {
        deferred.resolve({code: 'SUCCESS', data: []});
      }, 5000);

      return deferred.promise;
    }

    function getUsers() {
      var deferred = $q.defer();
      var httpResource = Rest.resource('user').post('getUsers');
      httpResource().then(function(result) {
         log.info('in resolve ', result);
         deferred.resolve(result);
        })
        .catch(function(err) {
          log.info('in reject ', err);
          deferred.reject(err);
        });
      return deferred.promise;
    }

    function add(item) {
       var deferred = $q.defer();

       item.isActive = 'true';
       console.log('item',item);

       var httpResource = Rest.resource('user').post('createuser');
       httpResource(item).then(function(result) {
         log.info('in resolve 3', result);
         deferred.resolve(result);
       })
            .catch(function(err) {
              log.info('in reject ', err);
              deferred.reject(err);
            });

       return deferred.promise;
     }

    function update(item) {
       var deferred = $q.defer();

       console.log('item',item);

        var httpResource = Rest.resource('user').post('update');
        httpResource(item).then(function(result) {
         log.info('in resolve 3', result);
         deferred.resolve(result);
        })
        .catch(function(err) {
          log.info('in reject ', err);
          deferred.reject(err);
        });

       return deferred.promise;
     }

     function changePassword(user) {
        var deferred = $q.defer();

        console.log('item',item);

        var httpResource = Rest.resource('user').post('changePassword');
        httpResource(item).then(function(result) {
           log.info('in resolve 3', result);
           deferred.resolve(result);
        })
        .catch(function(err) {
            log.info('in reject ', err);
            deferred.reject(err);
        });

         return deferred.promise;
     }

  }
});
