/**
 * @namespace act.Main
 */
define([
   'app/main-admins/services/admins.service',
   'app/services/dialog.service',

  ], function() {

    angular
      .module('act.Main.Admins')
      .controller('MainAdminsBaseController', controller);

    controller.$inject = ['$rootScope','$state', 'logger', 'ToastService', 'AdminsService','DialogService'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($rootScope, $state, logger, ToastService, AdminsService, DialogService) {
    var ViewModel = this;
    //     window.onbeforeunload = function(e) {
    //     e = e || window.event;
    //     e.preventDefault = true;
    //     e.cancelBubble = true;
    //     e.returnValue = 'test';
    // };
    $rootScope.pageTitle = 'Admins';

    ViewModel.showAddDialog = showAddDialog;
    ViewModel.showUpdateDialog = showUpdateDialog;
    ViewModel.toggleActiveUser = toggleActiveUser;
    ViewModel.showChangePassDialog = showChangePassDialog;

    
    ViewModel.selected = [];
    ViewModel.users = [];
    ViewModel.loadPromise = false;
    ViewModel.getUser = function() {
        AdminsService.getAdmins().then(function(result) {
            var obj = result.data.data;
            ViewModel.users = obj;
            // console.log("user",ViewModel.users);

            return obj;
        });

    };

    ViewModel.add = function(user) {
      AdminsService.add(user).then(function(result) {
          var obj = result.data.data;
          ViewModel.users = obj;
          console.log("user",ViewModel.users);
          ViewModel.closeDialog();
          $state.reload();

          return obj;
        });

    };

    function showAddDialog(event) {

      DialogService.show({
        controller: 'MainAdminsBaseController',
        controllerPath: 'app/main-admins/ctrl/base.ctrl.js',
        templatePath: 'app/main-admins/tpl/addAdmin.tpl.html',
        options: {
          targetEvent: event
        }
      });
    }

    function showUpdateDialog(event, db) {

      DialogService.show({
        controller: 'MainAdminsController',
        controllerPath: 'app/main-admins/ctrl/admin.ctrl.js',
        templatePath: 'app/main-admins/tpl/addAdmin.tpl.html',
        options: {
          targetEvent: event
        },locals: db

      });
    }

    function showChangePassDialog(event, user) {
        DialogService.show({
          controller: 'ChangePassController',
          controllerPath: 'app/main-admins/ctrl/changePass.ctrl.js',
          templatePath: 'app/main-admins/tpl/changePass.tpl.html',
          options: {
            targetEvent: event
          },locals: user

        });

    }


    //Active/Deactivate User
    function toggleActiveUser(user_id, _isActive) {

        var user = {
            id: user_id,
            isActive: _isActive
        };
        AdminsService.update(user).then(function(result) {
          var resp = result.data;
          if (resp.status === 'success') {
              ToastService.success(resp.message);
              $state.reload();
          } else {
              ToastService.error(resp.message);
              $state.reload();
          }
          console.log("Result: ", result);
          return resp;
        });
    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
      DialogService.cancel();
    }

  }

  });
