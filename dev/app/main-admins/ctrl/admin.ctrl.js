/**
 * @namespace act.Main
 */
define([
  'app/main-admins/services/admins.service',
   'app/services/dialog.service',
  ], function() {

    angular
      .module('act.Main.Admins')
      .controller('MainAdminsController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'AdminsService','DialogService','localsData'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     shoaibmerchant
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, AdminsService, DialogService, localsData) {
    var ViewModel = this;
    ViewModel.user = localsData;
    ViewModel.update = update;


    ViewModel.selected = [];
    ViewModel.users = [];
    ViewModel.loadPromise = false;
    function update(user) {
      AdminsService.update(user).then(function(result) {
        var resp = result.data;
        if (resp.status === 'success') {
            ToastService.success(resp.message);
            ViewModel.closeDialog();
        } else {
            ToastService.error(resp.message);
            $state.reload();
            ViewModel.closeDialog();
        }
        console.log("Result: ", result);
        return resp;
      });

    }

    ViewModel.closeDialog = closeDialog;

    function closeDialog() {
      DialogService.cancel();
    }

  }

  });
