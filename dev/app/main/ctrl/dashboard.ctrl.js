/**
 * @namespace act.Main
 */
define([
], function () {

	angular
		.module('act.Main')
		.controller('MainDashboardController', controller);

	controller.$inject = ['$state', 'logger', 'ToastService','Rest'];

	return controller;

	// //////////////////////////////////////////////////////

	/**
	 * Main Dashboard Controller
	 *
	 * @public
	 *
	 * @memberof   act.Main
	 *
	 * @author     shoaibmerchant
	 */
	function controller($state, logger, ToastService, Rest) {
		var ViewModel = this;
		var log = logger.log().child('MainDashboardController');

        //Variable Declaration
        ViewModel.admins_count = 0;
        ViewModel.makers_count = 0;

        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

        /**
        *   GetUsersCount
        *
        */
        function getUsersCount() {
            var httpResource = Rest.resource('user').get('getUsersCount');
            httpResource().then(function(resp) {
                if (resp.data.status === 'success') {
                    console.log("Response:", resp);
                    ViewModel.admins_count = resp.data.count;
                } else {
                    ToastService.error("Something Went wrong.");
                }
            })
            .catch(function(err) {
                console.log("Error: ",err);
            });
        }



        /**
        *   GetMakersCount
        *
        */
        function getMakersCount() {
            var httpResource = Rest.resource('maker').get('getMakersCount');
            httpResource().then(function(resp) {
                if (resp.data.status === 'success') {
                    console.log("Response:", resp);
                    ViewModel.makers_count = resp.data.count;
                } else {
                    ToastService.error("Something Went wrong.");
                }
            })
            .catch(function(err) {
                console.log("Error: ",err);
            });
        }

        /**
        *   GetCarsCount
        *
        */
        function getCarsCount() {
            var httpResource = Rest.resource('car').get('getCarsCount');
            httpResource().then(function(resp) {
                if (resp.data.status === 'success') {
                    console.log("Response:", resp);
                    ViewModel.cars_count = resp.data.count;
                } else {
                    ToastService.error("Something Went wrong.");
                }
            })
            .catch(function(err) {
                console.log("Error: ",err);
            });
        }

        /**
        *   getDealersCount
        *
        */
        function getDealersCount() {
            var httpResource = Rest.resource('dealer').get('getDealersCount');
            httpResource().then(function(resp) {
                if (resp.data.status === 'success') {
                    console.log("Response:", resp);
                    ViewModel.dealers_count = resp.data.count;
                } else {
                    ToastService.error("Something Went wrong.");
                }
            })
            .catch(function(err) {
                console.log("Error: ",err);
            });
        }
        //CallBacks
        getUsersCount();
        getMakersCount();
        getCarsCount();
        getDealersCount();
        //=---==-=-=-=-=-=-=-=--==--=--=
	}
});
